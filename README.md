# MyERP

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=devcodex_oc_project9)

![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=devcodex_oc_project9&metric=alert_status)
![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=devcodex_oc_project9&metric=ncloc)
![Coverage](https://sonarcloud.io/api/project_badges/measure?project=devcodex_oc_project9&metric=coverage)
![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=devcodex_oc_project9&metric=vulnerabilities)

## Organisation du répertoire

* `docker` : répertoire relatifs aux conteneurs _docker_ utiles pour le projet
    *   `dev` : environnement de développement
* `myerp-business`: module avec la couche business de l'application
* `myerp-consumer`: module avec la couche consumer de l'application
* `myerp-model`: module avec la couche model de l'application
* `myerp-technical`: module avec la couche technical de l'application
* `src` : code source de l'application
* `tests`: module de analyse de code pour SonarCloud


## Environnement de développement

Les composants nécessaires lors du développement sont disponibles via des conteneurs _docker_.
L'environnement de développement est assemblé grâce à _docker-compose_
(cf docker/dev/docker-compose.yml).

Il comporte :

*   une base de données _PostgreSQL_ contenant un jeu de données de démo (`postgresql://127.0.0.1:9032/db_myerp`)



### Lancement

    cd docker/dev
    docker-compose up


### Arrêt

    cd docker/dev
    docker-compose stop


### Remise à zero

    cd docker/dev
    docker-compose stop
    docker-compose rm -v
    docker-compose up

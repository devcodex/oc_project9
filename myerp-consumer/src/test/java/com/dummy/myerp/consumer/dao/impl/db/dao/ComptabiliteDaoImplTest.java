package com.dummy.myerp.consumer.dao.impl.db.dao;

import com.dummy.myerp.consumer.dao.impl.db.rowmapper.comptabilite.*;
import com.dummy.myerp.consumer.db.DataSourcesEnum;
import com.dummy.myerp.model.bean.comptabilite.*;
import com.dummy.myerp.technical.exception.NotFoundException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ComptabiliteDaoImplTest {

    ApplicationContext ctx = new
            ClassPathXmlApplicationContext("com/dummy/myerp/consumer/applicationContext-consumer.xml");

    ComptabiliteDaoImpl comptabiliteDao = spy(ctx.getBean(ComptabiliteDaoImpl.class));

    @Mock
    JdbcTemplate jdbcTemplate;
    @Mock
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    ArgumentCaptor<String> sqlArgument;
    ArgumentCaptor<MapSqlParameterSource> sqlParamsArgument;

    @BeforeEach
    void initEach(TestInfo info) {
        if(info.getDisplayName().equals("Verify initialization of jdbcTemplates")) {
            return;
        }
        comptabiliteDao.setJdbcTemplate(jdbcTemplate);
        comptabiliteDao.setNamedParameterJdbcTemplate(namedParameterJdbcTemplate);

        sqlArgument = ArgumentCaptor.forClass(String.class);
        sqlParamsArgument = ArgumentCaptor.forClass(MapSqlParameterSource.class);

        doNothing().when(comptabiliteDao).initJdbcTemplate();
        doNothing().when(comptabiliteDao).initNamedJdbcTemplate();
    }

    @Test
    @DisplayName("getListCompteComptable Test")
    void getListCompteComptable() {
        ArgumentCaptor<CompteComptableRM> rowMapperArgument = ArgumentCaptor.forClass(CompteComptableRM.class);

        when(jdbcTemplate.query(sqlArgument.capture(), rowMapperArgument.capture())).thenReturn(null);

        comptabiliteDao.getListCompteComptable();

        assertThat(sqlArgument.getValue()).isEqualTo("SELECT * FROM myerp.compte_comptable");
        assertThat(comptabiliteDao.getSqlGetListCompteComptable()).isEqualTo("SELECT * FROM myerp.compte_comptable");

        verify(jdbcTemplate).query("SELECT * FROM myerp.compte_comptable", rowMapperArgument.getValue());
    }

    @Test
    @DisplayName("getListJournalComptable Test")
    void getListJournalComptable() {
        ArgumentCaptor<JournalComptableRM> rowMapperArgument = ArgumentCaptor.forClass(JournalComptableRM.class);

        when(jdbcTemplate.query(sqlArgument.capture(), rowMapperArgument.capture())).thenReturn(null);

        comptabiliteDao.getListJournalComptable();

        assertThat(sqlArgument.getValue()).isEqualTo("SELECT * FROM myerp.journal_comptable");
        assertThat(comptabiliteDao.getSqlGetListJournalComptable()).isEqualTo("SELECT * FROM myerp.journal_comptable");

        verify(jdbcTemplate).query("SELECT * FROM myerp.journal_comptable", rowMapperArgument.getValue());
    }

    @Test
    @DisplayName("getListEcritureComptable Test")
    void getListEcritureComptable() {
        ArgumentCaptor<EcritureComptableRM> rowMapperArgument = ArgumentCaptor.forClass(EcritureComptableRM.class);

        when(jdbcTemplate.query(sqlArgument.capture(), rowMapperArgument.capture())).thenReturn(null);

        comptabiliteDao.getListEcritureComptable();

        assertThat(sqlArgument.getValue()).isEqualTo("SELECT * FROM myerp.ecriture_comptable");
        assertThat(comptabiliteDao.getSqlGetListEcritureComptable()).isEqualTo("SELECT * FROM myerp.ecriture_comptable");

        verify(jdbcTemplate).query("SELECT * FROM myerp.ecriture_comptable", rowMapperArgument.getValue());
    }

    @Nested
    @DisplayName("getEcritureComptable Tests")
    public class getEcritureComptableTests {

        @Test
        @DisplayName("Nominal case")
        void getEcritureComptable() throws Exception {
            ArgumentCaptor<EcritureComptableRM> rowMapperArgument = ArgumentCaptor.forClass(EcritureComptableRM.class);

            when(namedParameterJdbcTemplate.queryForObject(
                    sqlArgument.capture(),
                    sqlParamsArgument.capture(),
                    rowMapperArgument.capture()
            )).thenReturn(null);

            EcritureComptable res = comptabiliteDao.getEcritureComptable(1);

            assertThat(sqlArgument.getValue()).isEqualTo("SELECT * FROM myerp.ecriture_comptable WHERE id = :id");
            assertThat(comptabiliteDao.getSqlGetEcritureComptable()).isEqualTo("SELECT * FROM myerp.ecriture_comptable WHERE id = :id");
            assertThat(sqlParamsArgument.getValue().getValue("id")).isEqualTo(1);

            verify(namedParameterJdbcTemplate).queryForObject("SELECT * FROM myerp.ecriture_comptable WHERE id = :id", sqlParamsArgument.getValue(), rowMapperArgument.getValue());
        }

        @Test
        @DisplayName("Throws not found exception")
        void getEcritureComptableThrowsNotFound() throws NotFoundException {
            ArgumentCaptor<EcritureComptableRM> rowMapperArgument = ArgumentCaptor.forClass(EcritureComptableRM.class);

            when(namedParameterJdbcTemplate.queryForObject(
                    sqlArgument.capture(),
                    sqlParamsArgument.capture(),
                    rowMapperArgument.capture()
            )).thenThrow(new EmptyResultDataAccessException(1));

            assertThatExceptionOfType(NotFoundException.class).isThrownBy(() -> {
                EcritureComptable res = comptabiliteDao.getEcritureComptable(999);
            }).withMessage("EcritureComptable non trouvée : id=999");

            assertThat(sqlArgument.getValue()).isEqualTo("SELECT * FROM myerp.ecriture_comptable WHERE id = :id");
            assertThat(comptabiliteDao.getSqlGetEcritureComptable()).isEqualTo("SELECT * FROM myerp.ecriture_comptable WHERE id = :id");
            assertThat(sqlParamsArgument.getValue().getValue("id")).isEqualTo(999);

            verify(namedParameterJdbcTemplate).queryForObject("SELECT * FROM myerp.ecriture_comptable WHERE id = :id", sqlParamsArgument.getValue(), rowMapperArgument.getValue());
        }
    }

    @Nested
    @DisplayName("getEcritureComptableByRef Tests")
    public class getEcritureComptableByRefTests {

        @Test
        @DisplayName("Nominal case")
        void getEcritureComptableByRef() throws Exception {
            ArgumentCaptor<EcritureComptableRM> rowMapperArgument = ArgumentCaptor.forClass(EcritureComptableRM.class);

            when(namedParameterJdbcTemplate.queryForObject(
                    sqlArgument.capture(),
                    sqlParamsArgument.capture(),
                    rowMapperArgument.capture()
            )).thenReturn(null);

            EcritureComptable res = comptabiliteDao.getEcritureComptableByRef("1");

            assertThat(sqlArgument.getValue()).isEqualTo("SELECT * FROM myerp.ecriture_comptable WHERE reference = :reference");
            assertThat(comptabiliteDao.getSqlGetEcritureComptableByRef()).isEqualTo("SELECT * FROM myerp.ecriture_comptable WHERE reference = :reference");
            assertThat(sqlParamsArgument.getValue().getValue("reference")).isEqualTo("1");

            verify(namedParameterJdbcTemplate).queryForObject("SELECT * FROM myerp.ecriture_comptable WHERE reference = :reference", sqlParamsArgument.getValue(), rowMapperArgument.getValue());
        }

        @Test
        @DisplayName("Throws not found exception")
        void getEcritureComptableByRefThrowsNotFound() throws NotFoundException {
            ArgumentCaptor<EcritureComptableRM> rowMapperArgument = ArgumentCaptor.forClass(EcritureComptableRM.class);

            when(namedParameterJdbcTemplate.queryForObject(
                    sqlArgument.capture(),
                    sqlParamsArgument.capture(),
                    rowMapperArgument.capture()
            )).thenThrow(new EmptyResultDataAccessException(1));

            assertThatExceptionOfType(NotFoundException.class).isThrownBy(() -> {
                EcritureComptable res = comptabiliteDao.getEcritureComptableByRef("999");
            }).withMessage("EcritureComptable non trouvée : reference=999");

            assertThat(sqlArgument.getValue()).isEqualTo("SELECT * FROM myerp.ecriture_comptable WHERE reference = :reference");
            assertThat(comptabiliteDao.getSqlGetEcritureComptableByRef()).isEqualTo("SELECT * FROM myerp.ecriture_comptable WHERE reference = :reference");
            assertThat(sqlParamsArgument.getValue().getValue("reference")).isEqualTo("999");

            verify(namedParameterJdbcTemplate).queryForObject(
                    "SELECT * FROM myerp.ecriture_comptable WHERE reference = :reference",
                    sqlParamsArgument.getValue(),
                    rowMapperArgument.getValue()
            );
        }
    }

    @Test
    @DisplayName("getSequenceEcritureComptable Test")
    void getSequenceEcritureComptable() {
        ArgumentCaptor<SequenceEcritureComptableRM> rowMapperArgument = ArgumentCaptor.forClass(SequenceEcritureComptableRM.class);

        when(namedParameterJdbcTemplate.query(sqlArgument.capture(), sqlParamsArgument.capture(), rowMapperArgument.capture())).thenReturn(null);

        comptabiliteDao.getSequenceEcritureComptable("BQ", 2019);

        assertThat(sqlArgument.getValue())
                .isEqualTo("SELECT * FROM myerp.sequence_ecriture_comptable" +
                    " WHERE journal_code = :journalCode AND annee = :annee"
                );
        assertThat(comptabiliteDao.getSqlGetSequenceEcritureComptable())
                .isEqualTo("SELECT * FROM myerp.sequence_ecriture_comptable" +
                " WHERE journal_code = :journalCode AND annee = :annee"
                );
        assertThat(sqlParamsArgument.getValue().getValue("journalCode")).isEqualTo("BQ");
        assertThat(sqlParamsArgument.getValue().getValue("annee")).isEqualTo(2019);

        verify(namedParameterJdbcTemplate).query(
                "SELECT * FROM myerp.sequence_ecriture_comptable" +
                " WHERE journal_code = :journalCode AND annee = :annee",
                sqlParamsArgument.getValue(),
                rowMapperArgument.getValue()
        );
    }

    @Test
    @DisplayName("loadListLigneEcriture Test")
    void loadListLigneEcriture() {
        ArgumentCaptor<LigneEcritureComptableRM> rowMapperArgument = ArgumentCaptor.forClass(LigneEcritureComptableRM.class);
        List<LigneEcritureComptable> ligneEcritureComptableList = Arrays.asList(new LigneEcritureComptable(), new LigneEcritureComptable());
        EcritureComptable ecritureComptable = spy(EcritureComptable.class);
        ecritureComptable.setId(1);

        when(namedParameterJdbcTemplate.query(sqlArgument.capture(), sqlParamsArgument.capture(), rowMapperArgument.capture())).thenReturn(ligneEcritureComptableList);

        comptabiliteDao.loadListLigneEcriture(ecritureComptable);

        assertThat(sqlArgument.getValue())
                .isEqualTo("SELECT * FROM myerp.ligne_ecriture_comptable " +
                        "WHERE ecriture_id = :ecriture_id ORDER BY ligne_id"
                );
        assertThat(comptabiliteDao.getSqlLoadListLigneEcriture())
                .isEqualTo("SELECT * FROM myerp.ligne_ecriture_comptable " +
                        "WHERE ecriture_id = :ecriture_id ORDER BY ligne_id"
                );
        assertThat(sqlParamsArgument.getValue().getValue("ecriture_id")).isEqualTo(1);

        verify(namedParameterJdbcTemplate).query(
                "SELECT * FROM myerp.ligne_ecriture_comptable " +
                        "WHERE ecriture_id = :ecriture_id ORDER BY ligne_id",
                sqlParamsArgument.getValue(),
                rowMapperArgument.getValue()
        );
        verify(ecritureComptable, times(2)).getListLigneEcriture();
    }

    @Test
    @DisplayName("insertEcritureComptable Test")
    void insertEcritureComptable() throws Exception {
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2020");
        EcritureComptable ecritureComptable = new EcritureComptable();
        ecritureComptable.setLibelle("Libelle");
        ecritureComptable.setDate(new java.sql.Date(date.getTime()));
        ecritureComptable.setJournal(new JournalComptable("BQ", "Banque"));
        ecritureComptable.setReference("BQ-2020/00001");

        when(namedParameterJdbcTemplate.update(sqlArgument.capture(), sqlParamsArgument.capture())).thenReturn(1);
        when(jdbcTemplate.queryForObject("SELECT last_value FROM myerp.ecriture_comptable_id_seq", Integer.class)).thenReturn(1);

        comptabiliteDao.insertEcritureComptable(ecritureComptable);

        assertThat(sqlArgument.getValue())
                .isEqualTo("INSERT INTO myerp.ecriture_comptable (id, journal_code, reference, date, libelle) " +
                        "VALUES (nextval('myerp.ecriture_comptable_id_seq'), :journalCode, :reference, :date, :libelle)"
                );
        assertThat(comptabiliteDao.getSqlInsertEcritureComptable())
                .isEqualTo("INSERT INTO myerp.ecriture_comptable (id, journal_code, reference, date, libelle) " +
                        "VALUES (nextval('myerp.ecriture_comptable_id_seq'), :journalCode, :reference, :date, :libelle)"
                );
        assertThat(sqlParamsArgument.getValue().getValue("journalCode")).isEqualTo("BQ");
        assertThat(sqlParamsArgument.getValue().getValue("reference")).isEqualTo("BQ-2020/00001");
        assertThat(sqlParamsArgument.getValue().getValue("date")).isEqualTo(date);
        assertThat(sqlParamsArgument.getValue().getValue("libelle")).isEqualTo("Libelle");

        verify(namedParameterJdbcTemplate).update(sqlArgument.getValue(), sqlParamsArgument.getValue());
        verify(jdbcTemplate).queryForObject("SELECT last_value FROM myerp.ecriture_comptable_id_seq", Integer.class);
    }

    @Test
    @DisplayName("insertListLigneEcritureComptable Test")
    void insertListLigneEcritureComptable(){
        EcritureComptable ecritureComptable = new EcritureComptable();
        ecritureComptable.setId(1);
        ecritureComptable.getListLigneEcriture().addAll(Arrays.asList(
                new LigneEcritureComptable(new CompteComptable(1),null, new BigDecimal(123),null),
                new LigneEcritureComptable(new CompteComptable(2),null, null, new BigDecimal(123)
        )));

        when(namedParameterJdbcTemplate.update(sqlArgument.capture(), sqlParamsArgument.capture())).thenReturn(1);

        comptabiliteDao.insertListLigneEcritureComptable(ecritureComptable);

        assertThat(sqlArgument.getValue())
                .isEqualTo("INSERT INTO myerp.ligne_ecriture_comptable (ecriture_id, ligne_id, compte_comptable_numero, libelle, debit, credit)" +
                        " VALUES (:ecriture_id, :ligne_id, :compte_comptable_numero, :libelle, :debit, :credit)"
                );
        assertThat(comptabiliteDao.getSqlInsertListLigneEcritureComptable())
                .isEqualTo("INSERT INTO myerp.ligne_ecriture_comptable (ecriture_id, ligne_id, compte_comptable_numero, libelle, debit, credit)" +
                        " VALUES (:ecriture_id, :ligne_id, :compte_comptable_numero, :libelle, :debit, :credit)"
                );
        assertThat(sqlParamsArgument.getValue().getValue("ecriture_id")).isEqualTo(1);

        verify(namedParameterJdbcTemplate, times(2)).update(sqlArgument.getValue(), sqlParamsArgument.getValue());
    }

    @Test
    @DisplayName("updateEcritureComptable Test")
    void updateEcritureComptable() throws Exception{
        ComptabiliteDaoImpl spyComptabiliteDao = spy(comptabiliteDao);
        doNothing().when(spyComptabiliteDao).initNamedJdbcTemplate();
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2020");
        EcritureComptable ecritureComptable = new EcritureComptable();
        ecritureComptable.setLibelle("Libelle");
        ecritureComptable.setDate(new java.sql.Date(date.getTime()));
        ecritureComptable.setJournal(new JournalComptable("BQ", "Banque"));
        ecritureComptable.setReference("BQ-2020/00001");

        when(namedParameterJdbcTemplate.update(sqlArgument.capture(), sqlParamsArgument.capture())).thenReturn(1);
        doNothing().when(spyComptabiliteDao).deleteListLigneEcritureComptable(ecritureComptable.getId());
        doNothing().when(spyComptabiliteDao).insertListLigneEcritureComptable(ecritureComptable);

        spyComptabiliteDao.updateEcritureComptable(ecritureComptable);

        assertThat(sqlArgument.getValue())
                .isEqualTo("UPDATE myerp.ecriture_comptable " +
                        "SET journal_code = :journalCode, reference = :reference, date = :date, libelle = :libelle WHERE id = :id"
                );
        assertThat(spyComptabiliteDao.getSqlUpdateEcritureComptable())
                .isEqualTo("UPDATE myerp.ecriture_comptable " +
                        "SET journal_code = :journalCode, reference = :reference, date = :date, libelle = :libelle WHERE id = :id"
                );

        assertThat(sqlParamsArgument.getValue().getValue("journalCode")).isEqualTo("BQ");
        assertThat(sqlParamsArgument.getValue().getValue("reference")).isEqualTo("BQ-2020/00001");
        assertThat(sqlParamsArgument.getValue().getValue("date")).isEqualTo(date);
        assertThat(sqlParamsArgument.getValue().getValue("libelle")).isEqualTo("Libelle");

        verify(namedParameterJdbcTemplate).update(sqlArgument.getValue(), sqlParamsArgument.getValue());
        verify(spyComptabiliteDao).deleteListLigneEcritureComptable(ecritureComptable.getId());
        verify(spyComptabiliteDao).insertListLigneEcritureComptable(ecritureComptable);
    }

    @Test
    @DisplayName("deleteEcritureComptable Test")
    void deleteEcritureComptable() {

        when(namedParameterJdbcTemplate.update(sqlArgument.capture(), sqlParamsArgument.capture())).thenReturn(1);

        comptabiliteDao.deleteEcritureComptable(5);

        assertThat(sqlArgument.getValue())
                .isEqualTo("DELETE FROM myerp.ecriture_comptable WHERE id = :id");
        assertThat(comptabiliteDao.getSqlDeleteEcritureComptable())
                .isEqualTo("DELETE FROM myerp.ecriture_comptable WHERE id = :id");
        assertThat(sqlParamsArgument.getValue().getValue("id")).isEqualTo(5);

        verify(namedParameterJdbcTemplate).update(sqlArgument.getValue(), sqlParamsArgument.getValue());
    }

    @Test
    @DisplayName("deleteListLigneEcritureComptable Test")
    void deleteListLigneEcritureComptable() {
        when(namedParameterJdbcTemplate.update(sqlArgument.capture(), sqlParamsArgument.capture())).thenReturn(1);

        comptabiliteDao.deleteListLigneEcritureComptable(4);

        assertThat(sqlArgument.getValue())
                .isEqualTo("DELETE FROM myerp.ligne_ecriture_comptable WHERE ecriture_id = :ecriture_id");
        assertThat(comptabiliteDao.getSqlDeleteListLigneEcritureComptable())
                .isEqualTo("DELETE FROM myerp.ligne_ecriture_comptable WHERE ecriture_id = :ecriture_id");
        assertThat(sqlParamsArgument.getValue().getValue("ecriture_id")).isEqualTo(4);

        verify(namedParameterJdbcTemplate).update(sqlArgument.getValue(), sqlParamsArgument.getValue());
    }

    @Test
    @DisplayName("upsertSequenceEcritureComptable Test")
    void upsertSequenceEcritureComptable() {
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable();
        sequenceEcritureComptable.setCodeJournal("BQ");
        sequenceEcritureComptable.setAnnee(2020);
        sequenceEcritureComptable.setDerniereValeur(1);

        when(namedParameterJdbcTemplate.update(sqlArgument.capture(), sqlParamsArgument.capture())).thenReturn(1);

        comptabiliteDao.upsertSequenceEcritureComptable(sequenceEcritureComptable);

        assertThat(sqlArgument.getValue())
                .isEqualTo("INSERT INTO myerp.sequence_ecriture_comptable(journal_code, annee, derniere_valeur) " +
                        "VALUES (:journalCode, :annee, :derniereValeur) ON CONFLICT (journal_code, annee) " +
                        "DO UPDATE SET derniere_valeur = :derniereValeur"
                );
        assertThat(comptabiliteDao.getSqlUpsertSequenceEcritureComptable())
                .isEqualTo("INSERT INTO myerp.sequence_ecriture_comptable(journal_code, annee, derniere_valeur) " +
                        "VALUES (:journalCode, :annee, :derniereValeur) " +
                        "ON CONFLICT (journal_code, annee) " +
                        "DO UPDATE SET derniere_valeur = :derniereValeur"
                );
        assertThat(sqlParamsArgument.getValue().getValue("journalCode")).isEqualTo("BQ");
        assertThat(sqlParamsArgument.getValue().getValue("annee")).isEqualTo(2020);
        assertThat(sqlParamsArgument.getValue().getValue("derniereValeur")).isEqualTo(1);

        verify(namedParameterJdbcTemplate).update(sqlArgument.getValue(), sqlParamsArgument.getValue());
    }

    @Test
    @DisplayName("updateSequenceEcritureComptable Test")
    void updateSequenceEcritureComptable() {
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable();
        sequenceEcritureComptable.setCodeJournal("BQ");
        sequenceEcritureComptable.setAnnee(2020);
        sequenceEcritureComptable.setDerniereValeur(1);

        when(namedParameterJdbcTemplate.update(sqlArgument.capture(), sqlParamsArgument.capture())).thenReturn(1);

        comptabiliteDao.updateSequenceEcritureComptable(sequenceEcritureComptable);

        assertThat(sqlArgument.getValue())
                .isEqualTo("UPDATE myerp.sequence_ecriture_comptable SET derniere_valeur = :derniereValeur " +
                        "WHERE journal_code = :journalCode AND annee = :annee"
                );
        assertThat(comptabiliteDao.getSqlUpdateSequenceEcritureComptable())
                .isEqualTo("UPDATE myerp.sequence_ecriture_comptable SET derniere_valeur = :derniereValeur " +
                        "WHERE journal_code = :journalCode AND annee = :annee"
                );
        assertThat(sqlParamsArgument.getValue().getValue("journalCode")).isEqualTo("BQ");
        assertThat(sqlParamsArgument.getValue().getValue("annee")).isEqualTo(2020);
        assertThat(sqlParamsArgument.getValue().getValue("derniereValeur")).isEqualTo(1);

        verify(namedParameterJdbcTemplate).update(sqlArgument.getValue(), sqlParamsArgument.getValue());
    }

    @Test
    @DisplayName("Verify initialization of jdbcTemplates")
    void verifyInitJdbc() {
        comptabiliteDao.setJdbcTemplate(null);
        comptabiliteDao.setNamedParameterJdbcTemplate(null);

        comptabiliteDao.initJdbcTemplate();
        comptabiliteDao.initNamedJdbcTemplate();

        assertThat(comptabiliteDao.getJdbcTemplate()).isNotNull();
        assertThat(comptabiliteDao.getNamedParameterJdbcTemplate()).isNotNull();
    }
}

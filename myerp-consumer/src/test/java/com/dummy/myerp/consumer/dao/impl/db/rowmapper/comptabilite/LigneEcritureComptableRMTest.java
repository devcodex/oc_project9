package com.dummy.myerp.consumer.dao.impl.db.rowmapper.comptabilite;

import com.dummy.myerp.consumer.dao.impl.cache.CompteComptableDaoCache;
import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;
import com.dummy.myerp.model.bean.comptabilite.LigneEcritureComptable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.sql.ResultSet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LigneEcritureComptableRMTest {
    LigneEcritureComptableRM ligneEcritureComptableRM = new LigneEcritureComptableRM();

    @Mock
    CompteComptableDaoCache compteComptableDaoCache;
    @Mock
    ResultSet resultSet;

    @BeforeEach
    void initEach() {
        ligneEcritureComptableRM.setCompteComptableDaoCache(compteComptableDaoCache);
    }

    @Test
    @DisplayName("Test CompteComptable RowMapper")
    void mapRow() throws Exception {

        JournalComptable journalComptable = new JournalComptable("BQ", "Banque");
        CompteComptable compteComptable = mock(CompteComptable.class);


        when(resultSet.getObject("compte_comptable_numero", Integer.class)).thenReturn(606);
        when(resultSet.getBigDecimal("credit")).thenReturn(new BigDecimal(43));
        when(resultSet.getBigDecimal("debit")).thenReturn(null);
        when(resultSet.getString("libelle")).thenReturn("Cartouches d’imprimante");
        when(compteComptableDaoCache.getByNumero(606)).thenReturn(compteComptable);

        LigneEcritureComptable res = ligneEcritureComptableRM.mapRow(resultSet, 0);

        assertThat(res)
                .hasFieldOrPropertyWithValue("compteComptable", compteComptable)
                .hasFieldOrPropertyWithValue("credit", new BigDecimal(43))
                .hasFieldOrPropertyWithValue("debit", null)
                .hasFieldOrPropertyWithValue("libelle", "Cartouches d’imprimante");

        verify(resultSet).getObject("compte_comptable_numero", Integer.class);
        verify(resultSet).getBigDecimal("credit");
        verify(resultSet).getBigDecimal("debit");
        verify(resultSet).getString("libelle");
        verify(compteComptableDaoCache).getByNumero(606);
    }
}

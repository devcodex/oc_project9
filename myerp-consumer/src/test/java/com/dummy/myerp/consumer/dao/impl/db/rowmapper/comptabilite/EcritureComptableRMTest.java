package com.dummy.myerp.consumer.dao.impl.db.rowmapper.comptabilite;

import com.dummy.myerp.consumer.ConsumerHelper;
import com.dummy.myerp.consumer.dao.impl.DaoProxyImpl;
import com.dummy.myerp.consumer.dao.impl.cache.JournalComptableDaoCache;
import com.dummy.myerp.consumer.dao.impl.db.dao.ComptabiliteDaoImpl;
import com.dummy.myerp.model.bean.comptabilite.EcritureComptable;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EcritureComptableRMTest {
    EcritureComptableRM ecritureComptableRM = new EcritureComptableRM();

    @InjectMocks
    DaoProxyImpl daoProxy;

    @Mock
    ComptabiliteDaoImpl comptabiliteDao;
    @Mock
    JournalComptableDaoCache journalComptableDaoCache;
    @Mock
    ResultSet resultSet;

    @BeforeEach
    void initEach() {
        ConsumerHelper.configure(daoProxy);
        ecritureComptableRM.setJournalComptableDaoCache(journalComptableDaoCache);
    }

    @Test
    @DisplayName("Test CompteComptable RowMapper")
    void mapRow() throws Exception {

        Date date = new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2018");

        JournalComptable journalComptable = new JournalComptable("BQ", "Banque");

        when(resultSet.getInt("id")).thenReturn(1);
        when(resultSet.getString("journal_code")).thenReturn("BQ");
        when(resultSet.getString("reference")).thenReturn("BQ-2016/00001");
        when(resultSet.getDate("date")).thenReturn(new java.sql.Date(date.getTime()));
        when(resultSet.getString("libelle")).thenReturn("Cartouches d’imprimante");
        when(journalComptableDaoCache.getByCode("BQ")).thenReturn(journalComptable);

        doNothing().when(comptabiliteDao).loadListLigneEcriture(any(EcritureComptable.class));

        EcritureComptable res = ecritureComptableRM.mapRow(resultSet, 0);

        assertThat(res)
                .hasFieldOrPropertyWithValue("id", 1)
                .hasFieldOrPropertyWithValue("journal", journalComptable)
                .hasFieldOrPropertyWithValue("reference", "BQ-2016/00001")
                .hasFieldOrPropertyWithValue("date", date)
                .hasFieldOrPropertyWithValue("libelle", "Cartouches d’imprimante");

        verify(resultSet).getInt("id");
        verify(resultSet).getString("journal_code");
        verify(resultSet).getString("reference");
        verify(resultSet).getDate("date");
        verify(resultSet).getString("libelle");
        verify(journalComptableDaoCache).getByCode("BQ");
        verify(comptabiliteDao).loadListLigneEcriture(res);
    }
}

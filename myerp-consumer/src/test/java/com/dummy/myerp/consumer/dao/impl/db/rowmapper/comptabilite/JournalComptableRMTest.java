package com.dummy.myerp.consumer.dao.impl.db.rowmapper.comptabilite;

import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class JournalComptableRMTest {
    JournalComptableRM journalComptableRM = new JournalComptableRM();

    @Mock
    ResultSet resultSet;

    @Test
    @DisplayName("Test JournalComptable RowMapper")
    void mapRow() throws SQLException {
        when(resultSet.getString("code")).thenReturn("BQ");
        when(resultSet.getString("libelle")).thenReturn("Banque");

        JournalComptable res = journalComptableRM.mapRow(resultSet, 0);

        assertThat(res).hasFieldOrPropertyWithValue("code", "BQ").hasFieldOrPropertyWithValue("libelle", "Banque");

        verify(resultSet).getString("code");
        verify(resultSet).getString("libelle");
    }
}

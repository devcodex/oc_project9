package com.dummy.myerp.consumer.dao.impl.db.rowmapper.comptabilite;

import com.dummy.myerp.model.bean.comptabilite.SequenceEcritureComptable;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SequenceEcritureComptableRMTest {
    SequenceEcritureComptableRM  sequenceEcritureComptableRM = new SequenceEcritureComptableRM();

    @Mock
    ResultSet resultSet;

    @Test
    @DisplayName("Test JournalComptable RowMapper")
    void mapRow() throws SQLException {
        when(resultSet.getString("journal_code")).thenReturn("BQ");
        when(resultSet.getInt("annee")).thenReturn(2020);
        when(resultSet.getInt("derniere_valeur")).thenReturn(1);

        SequenceEcritureComptable res = sequenceEcritureComptableRM.mapRow(resultSet, 0);

        assertThat(res)
                .hasFieldOrPropertyWithValue("codeJournal", "BQ")
                .hasFieldOrPropertyWithValue("annee", 2020)
                .hasFieldOrPropertyWithValue("derniereValeur", 1);

        verify(resultSet).getString("journal_code");
        verify(resultSet).getInt("annee");
        verify(resultSet).getInt("derniere_valeur");
    }

}

package com.dummy.myerp.consumer.dao.impl.cache;

import com.dummy.myerp.consumer.ConsumerHelper;
import com.dummy.myerp.consumer.dao.impl.DaoProxyImpl;
import com.dummy.myerp.consumer.dao.impl.db.dao.ComptabiliteDaoImpl;
import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class JournalComptableDaoCacheTest {
    JournalComptableDaoCache journalComptableDaoCache = new JournalComptableDaoCache();

    @InjectMocks
    DaoProxyImpl daoProxy;

    @Mock
    ComptabiliteDaoImpl comptabiliteDao;

    List<JournalComptable> journalComptableList;

    @BeforeEach
    void initEach() {
        ConsumerHelper.configure(daoProxy);
        journalComptableList = new ArrayList<>();

        journalComptableList.addAll(
                Arrays.asList(
                        new JournalComptable("AC", "Achat"),
                        new JournalComptable("VE", "Vente"),
                        new JournalComptable("BQ", "Banque"),
                        new JournalComptable("OD", "Opérations Diverses")
                )
        );
    }

    @Test
    @DisplayName("Should fill CompteComptable list")
    void getByCodeFillsList() {
        journalComptableDaoCache.setListJournalComptable(null);

        when(comptabiliteDao.getListJournalComptable()).thenReturn(journalComptableList);

        journalComptableDaoCache.getByCode("AC");

        assertThat(journalComptableDaoCache.getListJournalComptable()).isEqualTo(journalComptableList);

        verify(comptabiliteDao).getListJournalComptable();
    }

    @Test
    @DisplayName("Should return JournalCompatable by code when JournalComptable list is empty")
    void getByCodeEmptyList() {
        journalComptableDaoCache.setListJournalComptable(null);

        when(comptabiliteDao.getListJournalComptable()).thenReturn(journalComptableList);

        JournalComptable res = journalComptableDaoCache.getByCode("AC");

        assertThat(res).hasFieldOrPropertyWithValue("code", "AC").hasFieldOrPropertyWithValue("libelle", "Achat");

        verify(comptabiliteDao).getListJournalComptable();
    }

    @Test
    @DisplayName("Should return null if number doesn't exist")
    void getByCodeFilledList() {

        journalComptableDaoCache.setListJournalComptable(journalComptableList);

        JournalComptable res = journalComptableDaoCache.getByCode("AC");

        assertThat(res).hasFieldOrPropertyWithValue("code", "AC").hasFieldOrPropertyWithValue("libelle", "Achat");

        verify(comptabiliteDao, never()).getListJournalComptable();
    }

    @Test
    @DisplayName("Should return null if number doesn't exist")
    void getByCodeReturnNull() {

        journalComptableDaoCache.setListJournalComptable(journalComptableList);

        JournalComptable res = journalComptableDaoCache.getByCode("ZZ");

        assertThat(res).isNull();

        verify(comptabiliteDao, never()).getListJournalComptable();
    }
}

package com.dummy.myerp.consumer.dao.impl.db.rowmapper.comptabilite;

import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CompteComptableRMTest {
    CompteComptableRM compteComptableRM = new CompteComptableRM();

    @Mock
    ResultSet resultSet;

    @Test
    @DisplayName("Test CompteComptable RowMapper")
    void mapRow() throws SQLException {

        when(resultSet.getInt("numero")).thenReturn(411);
        when(resultSet.getString("libelle")).thenReturn("Clients");

        CompteComptable res = compteComptableRM.mapRow(resultSet, 0);

        assertThat(res).hasFieldOrPropertyWithValue("numero", 411).hasFieldOrPropertyWithValue("libelle", "Clients");

        verify(resultSet).getInt("numero");
        verify(resultSet).getString("libelle");
    }
}

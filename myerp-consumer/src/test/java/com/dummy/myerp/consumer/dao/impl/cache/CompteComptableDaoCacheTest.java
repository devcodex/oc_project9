package com.dummy.myerp.consumer.dao.impl.cache;

import com.dummy.myerp.consumer.ConsumerHelper;
import com.dummy.myerp.consumer.dao.contrat.DaoProxy;
import com.dummy.myerp.consumer.dao.impl.DaoProxyImpl;
import com.dummy.myerp.consumer.dao.impl.db.dao.ComptabiliteDaoImpl;
import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.FieldSetter;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CompteComptableDaoCacheTest {

    CompteComptableDaoCache compteComptableDaoCache = new CompteComptableDaoCache();

    @InjectMocks
    DaoProxyImpl daoProxy;

    @Mock
    ComptabiliteDaoImpl comptabiliteDao;

    List<CompteComptable> compteComptableList;

    @BeforeEach
    void initEach() {
        ConsumerHelper.configure(daoProxy);
        compteComptableList = new ArrayList<>();

        compteComptableList.addAll(
                Arrays.asList(
                    new CompteComptable(401, "Fournisseurs"),
                    new CompteComptable(411, "Clients"),
                    new CompteComptable(4456, "Taxes sur le chiffre d''affaires déductibles"),
                    new CompteComptable(4457, "Taxes sur le chiffre d''affaires collectées par l''entreprise"),
                    new CompteComptable(512, "Banque")
                )
        );
    }

    @Test
    @DisplayName("Should fill CompteComptable list")
    void getByNumeroFillsList() {
        compteComptableDaoCache.setListCompteComptable(null);

        when(comptabiliteDao.getListCompteComptable()).thenReturn(compteComptableList);

        compteComptableDaoCache.getByNumero(411);

        assertThat(compteComptableDaoCache.getListCompteComptable()).isEqualTo(compteComptableList);

        verify(comptabiliteDao).getListCompteComptable();
    }

    @Test
    @DisplayName("Should return CompteCompatable by number when CompteComptable list is empty")
    void getByNumeroEmptyList() {
        compteComptableDaoCache.setListCompteComptable(null);

        when(comptabiliteDao.getListCompteComptable()).thenReturn(compteComptableList);

        CompteComptable res = compteComptableDaoCache.getByNumero(411);

        assertThat(res).hasFieldOrPropertyWithValue("numero", 411).hasFieldOrPropertyWithValue("libelle", "Clients");

        verify(comptabiliteDao).getListCompteComptable();
    }

    @Test
    @DisplayName("Should return null if number doesn't exist")
    void getByNumeroFilledList() {

        compteComptableDaoCache.setListCompteComptable(compteComptableList);

        CompteComptable res = compteComptableDaoCache.getByNumero(411);

        assertThat(res).hasFieldOrPropertyWithValue("numero", 411).hasFieldOrPropertyWithValue("libelle", "Clients");

        verify(comptabiliteDao, never()).getListCompteComptable();
    }

    @Test
    @DisplayName("Should return null if number doesn't exist")
    void getByNumeroReturnNull() {

        compteComptableDaoCache.setListCompteComptable(compteComptableList);

        CompteComptable res = compteComptableDaoCache.getByNumero(9999);

        assertThat(res).isNull();

        verify(comptabiliteDao, never()).getListCompteComptable();
    }
}

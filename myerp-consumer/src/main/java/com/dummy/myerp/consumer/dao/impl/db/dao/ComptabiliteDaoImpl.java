package com.dummy.myerp.consumer.dao.impl.db.dao;

import com.dummy.myerp.consumer.dao.contrat.ComptabiliteDao;
import com.dummy.myerp.consumer.dao.impl.db.rowmapper.comptabilite.*;
import com.dummy.myerp.consumer.db.AbstractDbConsumer;
import com.dummy.myerp.consumer.db.DataSourcesEnum;
import com.dummy.myerp.model.bean.comptabilite.*;
import com.dummy.myerp.technical.exception.NotFoundException;
import lombok.Getter;
import lombok.Setter;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.Types;
import java.util.List;


/**
 * Implémentation de l'interface {@link ComptabiliteDao}
 */
@Getter @Setter
public class ComptabiliteDaoImpl extends AbstractDbConsumer implements ComptabiliteDao {
    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    //Character literals constants
    private static final String JOURNAL_CODE = "journalCode";
    private static final String ANNEE = "annee";
    private static final String REFERENCE = "reference";
    private static final String DERNIERE_VALUER = "derniereValeur";
    private static final String ECRITURE_ID = "ecriture_id";
    private static final String LIBELLE = "libelle";

    // ==================== Constructeurs ====================
    /** Instance unique de la classe (design pattern Singleton) */
    private static final ComptabiliteDaoImpl INSTANCE = new ComptabiliteDaoImpl();

    /**
     * Renvoie l'instance unique de la classe (design pattern Singleton).
     *
     * @return {@link ComptabiliteDaoImpl}
     */
    public static ComptabiliteDaoImpl getInstance() {

        return ComptabiliteDaoImpl.INSTANCE;
    }

    /**
     * Constructeur.
     */
    protected ComptabiliteDaoImpl() {
        super();
    }


    // ==================== Méthodes ====================
    /** SQLgetListCompteComptable */
    private String sqlGetListCompteComptable;

    @Override
    public List<CompteComptable> getListCompteComptable() {
        initJdbcTemplate();
        CompteComptableRM vRM = new CompteComptableRM();
        return this.jdbcTemplate.query(sqlGetListCompteComptable, vRM);
    }


    /** SQLgetListJournalComptable */
    private String sqlGetListJournalComptable;

    @Override
    public List<JournalComptable> getListJournalComptable() {
        initJdbcTemplate();
        JournalComptableRM vRM = new JournalComptableRM();
        return this.jdbcTemplate.query(sqlGetListJournalComptable, vRM);
    }

    // ==================== EcritureComptable - GET ====================

    /** SQLgetListEcritureComptable */
    private String sqlGetListEcritureComptable;

    @Override
    public List<EcritureComptable> getListEcritureComptable() {
        initJdbcTemplate();
        EcritureComptableRM vRM = new EcritureComptableRM();
        return this.jdbcTemplate.query(sqlGetListEcritureComptable, vRM);
    }


    /** SQLgetEcritureComptable */
    private String sqlGetEcritureComptable;

    @Override
    public EcritureComptable getEcritureComptable(Integer pId) throws NotFoundException {
        initNamedJdbcTemplate();
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("id", pId);
        EcritureComptableRM vRM = new EcritureComptableRM();
        try {
            return this.namedParameterJdbcTemplate.queryForObject(sqlGetEcritureComptable, vSqlParams, vRM);
        } catch (EmptyResultDataAccessException vEx) {
            throw new NotFoundException("EcritureComptable non trouvée : id=" + pId);
        }
    }


    /** SQLgetEcritureComptableByRef */
    private String sqlGetEcritureComptableByRef;

    @Override
    public EcritureComptable getEcritureComptableByRef(String pReference) throws NotFoundException {
        initNamedJdbcTemplate();
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue(REFERENCE, pReference);
        EcritureComptableRM vRM = new EcritureComptableRM();
        try {
            return this.namedParameterJdbcTemplate.queryForObject(sqlGetEcritureComptableByRef, vSqlParams, vRM);
        } catch (EmptyResultDataAccessException vEx) {
            throw new NotFoundException("EcritureComptable non trouvée : reference=" + pReference);
        }
    }

    /** sqlGetSequenceEcritureComptable **/
    private String sqlGetSequenceEcritureComptable;

    @Override
    public List<SequenceEcritureComptable> getSequenceEcritureComptable(String journalCode, int annee) {
        initNamedJdbcTemplate();
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue(JOURNAL_CODE, journalCode);
        vSqlParams.addValue(ANNEE, annee);
        SequenceEcritureComptableRM vRM = new SequenceEcritureComptableRM();
        return this.namedParameterJdbcTemplate.query(sqlGetSequenceEcritureComptable, vSqlParams, vRM);
    }


    /** SQLloadListLigneEcriture */
    private String sqlLoadListLigneEcriture;

    @Override
    public void loadListLigneEcriture(EcritureComptable pEcritureComptable) {
        initNamedJdbcTemplate();
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue(ECRITURE_ID, pEcritureComptable.getId());
        LigneEcritureComptableRM vRM = new LigneEcritureComptableRM();
        List<LigneEcritureComptable> vList = this.namedParameterJdbcTemplate.query(sqlLoadListLigneEcriture, vSqlParams, vRM);
        pEcritureComptable.getListLigneEcriture().clear();
        pEcritureComptable.getListLigneEcriture().addAll(vList);
    }


    // ==================== EcritureComptable - INSERT ====================

    /** SQLinsertEcritureComptable */
    private String sqlInsertEcritureComptable;

    @Override
    public void insertEcritureComptable(EcritureComptable pEcritureComptable) {
        // ===== Ecriture Comptable
        initNamedJdbcTemplate();
        initJdbcTemplate();
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue(JOURNAL_CODE, pEcritureComptable.getJournal().getCode());
        vSqlParams.addValue(REFERENCE, pEcritureComptable.getReference());
        vSqlParams.addValue("date", pEcritureComptable.getDate(), Types.DATE);
        vSqlParams.addValue(LIBELLE, pEcritureComptable.getLibelle());

        this.namedParameterJdbcTemplate.update(sqlInsertEcritureComptable, vSqlParams);

        // ----- Récupération de l'id
        Integer vId = this.queryGetSequenceValuePostgreSQL(jdbcTemplate, "myerp.ecriture_comptable_id_seq",
                                                           Integer.class);
        pEcritureComptable.setId(vId);

        // ===== Liste des lignes d'écriture
        this.insertListLigneEcritureComptable(pEcritureComptable);
    }

    /** SQLinsertListLigneEcritureComptable */
    private String sqlInsertListLigneEcritureComptable;

    /**
     * Insert les lignes d'écriture de l'écriture comptable
     * @param pEcritureComptable l'écriture comptable
     */
    protected void insertListLigneEcritureComptable(EcritureComptable pEcritureComptable) {
        initNamedJdbcTemplate();
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue(ECRITURE_ID, pEcritureComptable.getId());

        int vLigneId = 0;
        for (LigneEcritureComptable vLigne : pEcritureComptable.getListLigneEcriture()) {
            vLigneId++;
            vSqlParams.addValue("ligne_id", vLigneId);
            vSqlParams.addValue("compte_comptable_numero", vLigne.getCompteComptable().getNumero());
            vSqlParams.addValue(LIBELLE, vLigne.getLibelle());
            vSqlParams.addValue("debit", vLigne.getDebit());

            vSqlParams.addValue("credit", vLigne.getCredit());

            this.namedParameterJdbcTemplate.update(sqlInsertListLigneEcritureComptable, vSqlParams);
        }
    }


    // ==================== EcritureComptable - UPDATE ====================

    /** SQLupdateEcritureComptable */
    private String sqlUpdateEcritureComptable;

    @Override
    public void updateEcritureComptable(EcritureComptable pEcritureComptable) {
        // ===== Ecriture Comptable
        initNamedJdbcTemplate();
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("id", pEcritureComptable.getId());
        vSqlParams.addValue(JOURNAL_CODE, pEcritureComptable.getJournal().getCode());
        vSqlParams.addValue(REFERENCE, pEcritureComptable.getReference());
        vSqlParams.addValue("date", pEcritureComptable.getDate(), Types.DATE);
        vSqlParams.addValue(LIBELLE, pEcritureComptable.getLibelle());

        this.namedParameterJdbcTemplate.update(sqlUpdateEcritureComptable, vSqlParams);

        // ===== Liste des lignes d'écriture
        this.deleteListLigneEcritureComptable(pEcritureComptable.getId());
        this.insertListLigneEcritureComptable(pEcritureComptable);
    }


    // ==================== EcritureComptable - DELETE ====================

    /** SQLdeleteEcritureComptable */
    private String sqlDeleteEcritureComptable;

    @Override
    public void deleteEcritureComptable(Integer pId) {
        // ===== Suppression des lignes d'écriture
        this.deleteListLigneEcritureComptable(pId);

        // ===== Suppression de l'écriture
        initNamedJdbcTemplate();
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue("id", pId);
        this.namedParameterJdbcTemplate.update(sqlDeleteEcritureComptable, vSqlParams);
    }

    /** SQLdeleteListLigneEcritureComptable */
    private String sqlDeleteListLigneEcritureComptable;

    /**
     * Supprime les lignes d'écriture de l'écriture comptable d'id {@code pEcritureId}
     * @param pEcritureId id de l'écriture comptable
     */
    protected void deleteListLigneEcritureComptable(Integer pEcritureId) {
        initNamedJdbcTemplate();
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue(ECRITURE_ID, pEcritureId);
        this.namedParameterJdbcTemplate.update(sqlDeleteListLigneEcritureComptable, vSqlParams);
    }

    // ==================== SequenceEcritureComptable - INSERT OR UPDATE ====================

    /** SQLupsertSequenceEcritureComptable */
    private String sqlUpsertSequenceEcritureComptable;

    @Override
    public void upsertSequenceEcritureComptable(SequenceEcritureComptable pSequenceEcritureComptable) {
        initNamedJdbcTemplate();
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue(JOURNAL_CODE, pSequenceEcritureComptable.getCodeJournal());
        vSqlParams.addValue(ANNEE, pSequenceEcritureComptable.getAnnee());
        vSqlParams.addValue(DERNIERE_VALUER, pSequenceEcritureComptable.getDerniereValeur());

        this.namedParameterJdbcTemplate.update(sqlUpsertSequenceEcritureComptable, vSqlParams);
    }

    /** SQLupdateSequenceEcritureComptable **/
    private String sqlUpdateSequenceEcritureComptable;

    @Override
    public void updateSequenceEcritureComptable(SequenceEcritureComptable pSequenceEcritureComptable) {
        initNamedJdbcTemplate();
        MapSqlParameterSource vSqlParams = new MapSqlParameterSource();
        vSqlParams.addValue(JOURNAL_CODE, pSequenceEcritureComptable.getCodeJournal());
        vSqlParams.addValue(ANNEE, pSequenceEcritureComptable.getAnnee());
        vSqlParams.addValue(DERNIERE_VALUER, pSequenceEcritureComptable.getDerniereValeur());

        this.namedParameterJdbcTemplate.update(sqlUpdateSequenceEcritureComptable, vSqlParams);
    }

    void initJdbcTemplate() {
        this.jdbcTemplate = new JdbcTemplate(this.getDataSource(DataSourcesEnum.MYERP));
    }

    void initNamedJdbcTemplate() {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(this.getDataSource(DataSourcesEnum.MYERP));
    }
}

package com.dummy.myerp.model.bean.comptabilite;

import org.apache.commons.lang3.ObjectUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.*;


public class EcritureComptableTest {

    private LigneEcritureComptable createLigne(Integer pCompteComptableNumero, String pDebit, String pCredit) {
        BigDecimal debit = pDebit == null ? null : new BigDecimal(pDebit);
        BigDecimal credit = pCredit == null ? null : new BigDecimal(pCredit);
        String vLibelle = ObjectUtils.defaultIfNull(debit, BigDecimal.ZERO)
                                     .subtract(ObjectUtils.defaultIfNull(credit, BigDecimal.ZERO)).toPlainString();
        return new LigneEcritureComptable(new CompteComptable(pCompteComptableNumero),
                                                                    vLibelle,
                                                                    debit, credit);
    }

    @Test
    @DisplayName("isEquilibree returns true")
    public void isEquilibree() {
        EcritureComptable ecriture = new EcritureComptable();

        ecriture.setLibelle("Equilibrée");
        ecriture.getListLigneEcriture().add(this.createLigne(1, "200.50", null));
        ecriture.getListLigneEcriture().add(this.createLigne(1, "100.50", "33"));
        ecriture.getListLigneEcriture().add(this.createLigne(2, null, "301"));
        ecriture.getListLigneEcriture().add(this.createLigne(2, "40", "7"));

        Assertions.assertTrue(ecriture.isEquilibree(), ecriture.toString());
    }

    @Test
    @DisplayName("isEquilibree returns false")
    public void isNotEquilibree() {
        EcritureComptable ecriture = new EcritureComptable();

        ecriture.setLibelle("Non équilibrée");
        ecriture.getListLigneEcriture().add(this.createLigne(1, "10", null));
        ecriture.getListLigneEcriture().add(this.createLigne(1, "20", "1"));
        ecriture.getListLigneEcriture().add(this.createLigne(2, null, "30"));
        ecriture.getListLigneEcriture().add(this.createLigne(2, "1", "2"));

        Assertions.assertFalse(ecriture.isEquilibree(), ecriture.toString());
    }

    @Test
    @DisplayName("getTotalCredit returns correct value")
    public void getTotalCredit() {
        EcritureComptable ecriture = new EcritureComptable();

        ecriture.setLibelle("Non équilibrée");
        ecriture.getListLigneEcriture().add(this.createLigne(1, null, "5"));
        ecriture.getListLigneEcriture().add(this.createLigne(1, null, "1"));
        ecriture.getListLigneEcriture().add(this.createLigne(2, null, "30"));
        ecriture.getListLigneEcriture().add(this.createLigne(2, null, "2"));

        Assertions.assertEquals(new BigDecimal(38), ecriture.getTotalCredit());
    }

    @Test
    @DisplayName("getTotalDebit returns correct value")
    public void getTotalDebit() {
        EcritureComptable ecriture = new EcritureComptable();

        ecriture.setLibelle("Non équilibrée");
        ecriture.getListLigneEcriture().add(this.createLigne(1, "10", null));
        ecriture.getListLigneEcriture().add(this.createLigne(1, "20", null));
        ecriture.getListLigneEcriture().add(this.createLigne(2, "5", null));
        ecriture.getListLigneEcriture().add(this.createLigne(2, "1", null));

        Assertions.assertEquals(new BigDecimal(36), ecriture.getTotalDebit());
    }

    @Nested
    @DisplayName("getYearFromDate Tests")
    class getYearFromDateTests {
        @Test
        @DisplayName("getYearFromDate returns 2018")
        public void getYearFromDate2018() throws Exception {
            EcritureComptable ecriture = new EcritureComptable();
            ecriture.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2018"));

            assertThat(ecriture.getYearFromDate()).isEqualTo(2018);
        }

        @Test
        @DisplayName("getYearFromDate returns true from Date.now()")
        public void getYearFromDateNow() throws Exception {
            EcritureComptable ecriture = new EcritureComptable();
            ecriture.setDate(new Date());

            int currentYear = Calendar.getInstance().get(Calendar.YEAR);

            assertThat(ecriture.getYearFromDate()).isEqualTo(currentYear);
        }
    }
}

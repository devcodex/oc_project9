package com.dummy.myerp.model.bean.comptabilite;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("Compte Comptable Tests")
class CompteComptableTest {

    private CompteComptable compteComptable;
    private List<CompteComptable> compteComptableList = Arrays.asList(
        new CompteComptable(0, "Compte Comptable 0"),
        new CompteComptable(1, "Compte Comptable 1"),
        new CompteComptable(2, "Compte Comptable 2"),
        new CompteComptable(),
        new CompteComptable(4, "Compte Comptable 4")
    );

    @BeforeEach
    public void beforeEachTest() {
        compteComptable = new CompteComptable();
    }

    @ParameterizedTest(name = "Returns ComptComptable with numero {0}")
    @ValueSource(ints = { 0, 1, 2, 4})
    @DisplayName("Returns Journal Comptable based on numero")
    void getByNumero(int arg) {
        compteComptable = CompteComptable.getByNumero(compteComptableList, arg);
        assertThat(compteComptable).isEqualTo(compteComptableList.get(arg));
    }

    @Test
    @DisplayName("Should return null when CompteComptable isn't present")
    void getByNumeroReturnsNull() {
        compteComptable = CompteComptable.getByNumero(compteComptableList, 10);
        assertThat(compteComptable).isNull();
    }
}
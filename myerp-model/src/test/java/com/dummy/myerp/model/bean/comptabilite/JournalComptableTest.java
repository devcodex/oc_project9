package com.dummy.myerp.model.bean.comptabilite;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
@DisplayName("Journal Comptable Tests")
class JournalComptableTest {

    private JournalComptable journalComptable;
    private List<JournalComptable> journalComptableList = Arrays.asList(
            new JournalComptable("12345", "Journal Comptable 12345"),
            new JournalComptable(),
            new JournalComptable("856", "Journal Comptable 856"),
            new JournalComptable("2125", "Journal Comptable 2125"),
            new JournalComptable("1", "Journal Comptable 1")
    );

    @BeforeEach
    public void beforeEachTest() {
        journalComptable = new JournalComptable();
    }

    @ParameterizedTest(name = "Returns Journal Comptable with code {0}")
    @ValueSource(strings = {"12345", "856", "2125", "1"})
    @DisplayName("Returns Journal Comptable based on code")
    void getByCode(String arg) {
        journalComptable = JournalComptable.getByCode(journalComptableList, arg);

        JournalComptable journalComptableTemporary = new JournalComptable();
        for (JournalComptable jc : journalComptableList) {
            if(jc.getCode() != null && jc.getCode().contains(arg)) {
                journalComptableTemporary = jc;
            }
        }
        assertThat(journalComptable).isEqualTo(journalComptableTemporary);
    }

    @Test
    @DisplayName("Should return null when JournalComptable isn't present")
    void getByCodeNull() {
        journalComptable = JournalComptable.getByCode(journalComptableList, "15975");
        assertThat(journalComptable).isNull();
    }
}
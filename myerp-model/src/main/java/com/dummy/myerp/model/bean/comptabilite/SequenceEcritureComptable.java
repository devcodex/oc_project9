package com.dummy.myerp.model.bean.comptabilite;


import lombok.*;

/**
 * Bean représentant une séquence pour les références d'écriture comptable
 */
@NoArgsConstructor @AllArgsConstructor
@Getter @Setter @ToString
public class SequenceEcritureComptable {

    // ==================== Attributs ====================
    /** Code journal comptable **/
    private String codeJournal;
    /** L'année */
    private Integer annee;
    /** La dernière valeur utilisée */
    private Integer derniereValeur;
}

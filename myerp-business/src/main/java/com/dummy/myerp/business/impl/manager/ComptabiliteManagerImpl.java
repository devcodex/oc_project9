package com.dummy.myerp.business.impl.manager;

import com.dummy.myerp.business.contrat.manager.ComptabiliteManager;
import com.dummy.myerp.business.impl.AbstractBusinessManager;
import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import com.dummy.myerp.model.bean.comptabilite.EcritureComptable;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;
import com.dummy.myerp.model.bean.comptabilite.SequenceEcritureComptable;
import com.dummy.myerp.technical.exception.FunctionalException;
import com.dummy.myerp.technical.exception.NotFoundException;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.TransactionStatus;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Set;


/**
 * Comptabilite manager implementation.
 */
@NoArgsConstructor
public class ComptabiliteManagerImpl extends AbstractBusinessManager implements ComptabiliteManager {


    // ==================== Getters/Setters ====================
    /**
     * {@inheritDoc}
     */
    @Override
    public List<CompteComptable> getListCompteComptable() {
        return getDaoProxy().getComptabiliteDao().getListCompteComptable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<JournalComptable> getListJournalComptable() {
        return getDaoProxy().getComptabiliteDao().getListJournalComptable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<EcritureComptable> getListEcritureComptable() {
        return getDaoProxy().getComptabiliteDao().getListEcritureComptable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void addReference(EcritureComptable ecritureComptable) {
        SequenceEcritureComptable sequenceEcritureComptable = new SequenceEcritureComptable();
        sequenceEcritureComptable.setCodeJournal(ecritureComptable.getJournal().getCode());
        sequenceEcritureComptable.setAnnee(ecritureComptable.getYearFromDate());

        List<SequenceEcritureComptable> listSequenceEcritureComptable =
                getDaoProxy().getComptabiliteDao().getSequenceEcritureComptable(
                        sequenceEcritureComptable.getCodeJournal(),
                        sequenceEcritureComptable.getAnnee()
                );

        int sequence = 0;
        if(!listSequenceEcritureComptable.isEmpty()) {
            for (SequenceEcritureComptable tempSequence : listSequenceEcritureComptable) {
                if(tempSequence.getDerniereValeur() > sequence) {
                    sequence = tempSequence.getDerniereValeur();
                }
            }
        }
        sequenceEcritureComptable.setDerniereValeur(sequence + 1);
        String lastVal = Integer.toString(sequenceEcritureComptable.getDerniereValeur());
        getDaoProxy().getComptabiliteDao().upsertSequenceEcritureComptable(sequenceEcritureComptable);
        StringBuilder reference = new StringBuilder();
        //Builds String with reference (Ex. BQ-2016/00003)
        reference.append(sequenceEcritureComptable.getCodeJournal())
                .append("-")
                .append(sequenceEcritureComptable.getAnnee())
                .append("/")
                //Creates 5 digit sequence value
                .append(("00000" + lastVal).substring(lastVal.length()));
        ecritureComptable.setReference(reference.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException {
        this.checkEcritureComptableUnit(pEcritureComptable);
        this.checkEcritureComptableContext(pEcritureComptable);
    }


    /**
     * Vérifie que l'Ecriture comptable respecte les règles de gestion unitaires,
     * c'est à dire indépendemment du contexte (unicité de la référence, exercie comptable non cloturé...)
     *
     * @param pEcritureComptable -
     * @throws FunctionalException Si l'Ecriture comptable ne respecte pas les règles de gestion
     */
    public void checkEcritureComptableUnit(EcritureComptable pEcritureComptable) throws FunctionalException {
        // ===== Vérification des contraintes unitaires sur les attributs de l'écriture,
        //          verifie aussi que une écriture comptable doit avoir au moins 2 lignes d'écriture (RG_Compta_3)
        Set<ConstraintViolation<EcritureComptable>> vViolations = getConstraintValidator().validate(pEcritureComptable);
        if (!vViolations.isEmpty()) {
            throw new FunctionalException("L'écriture comptable ne respecte pas les règles de gestion.",
                                          new ConstraintViolationException(
                                              "L'écriture comptable ne respecte pas les contraintes de validation",
                                              vViolations));
        }

        // ===== RG_Compta_2 : Pour qu'une écriture comptable soit valide, elle doit être équilibrée
        if (!pEcritureComptable.isEquilibree()) {
            throw new FunctionalException("L'écriture comptable n'est pas équilibrée.");
        }

        // To Do implemented - RG_Compta_5 : Format et contenu de la référence
        // vérifier que l'année dans la référence correspond bien à la date de l'écriture, idem pour le code journal...
        String[] breakDownReference = pEcritureComptable.getReference().split("[-/]");
        if(!breakDownReference[0].equals(pEcritureComptable.getJournal().getCode())) {
            throw new FunctionalException("Les donnes de la reference ne correspond pas au Journal Comptable associé");
        }
        if(!breakDownReference[1].equals(String.valueOf(pEcritureComptable.getYearFromDate()))) {
            throw new FunctionalException("Les donnes de la reference ne correspond pas à l'année associé");
        }
    }


    /**
     * Vérifie que l'Ecriture comptable respecte les règles de gestion liées au contexte
     * (unicité de la référence, année comptable non cloturé...)
     *
     * @param pEcritureComptable -
     * @throws FunctionalException Si l'Ecriture comptable ne respecte pas les règles de gestion
     */
    public void checkEcritureComptableContext(EcritureComptable pEcritureComptable) throws FunctionalException {
        // ===== RG_Compta_6 : La référence d'une écriture comptable doit être unique
        if(!StringUtils.isNoneEmpty(pEcritureComptable.getReference())) {
            throw new FunctionalException("La référence de l'écriture comptable n'est pas valide");
        }

        try {
            // Recherche d'une écriture ayant la même référence
            EcritureComptable vECRef = getDaoProxy().getComptabiliteDao().getEcritureComptableByRef(
                pEcritureComptable.getReference());

            // Si l'écriture à vérifier est une nouvelle écriture (id == null),
            // ou si elle ne correspond pas à l'écriture trouvée (id != idECRef),
            // c'est qu'il y a déjà une autre écriture avec la même référence
            if (pEcritureComptable.getId() == null
                || !pEcritureComptable.getId().equals(vECRef.getId())) {
                throw new FunctionalException("Une autre écriture comptable existe déjà avec la même référence.");
            }
        } catch (NotFoundException vEx) {
            // Dans ce cas, c'est bon, ça veut dire qu'on n'a aucune autre écriture avec la même référence.
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void insertEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException {
        this.checkEcritureComptable(pEcritureComptable);
        TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();
        try {
            getDaoProxy().getComptabiliteDao().insertEcritureComptable(pEcritureComptable);
            getTransactionManager().commitMyERP(vTS);
            vTS = null;
        } finally {
            getTransactionManager().rollbackMyERP(vTS);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateEcritureComptable(EcritureComptable pEcritureComptable) throws FunctionalException {
        this.checkEcritureComptable(pEcritureComptable);
        TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();
        try {
            getDaoProxy().getComptabiliteDao().updateEcritureComptable(pEcritureComptable);
            getTransactionManager().commitMyERP(vTS);
            vTS = null;
        } finally {
            getTransactionManager().rollbackMyERP(vTS);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteEcritureComptable(Integer pId) {
        TransactionStatus vTS = getTransactionManager().beginTransactionMyERP();
        try {
            getDaoProxy().getComptabiliteDao().deleteEcritureComptable(pId);
            getTransactionManager().commitMyERP(vTS);
            vTS = null;
        } finally {
            getTransactionManager().rollbackMyERP(vTS);
        }
    }
}

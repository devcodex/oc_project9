package com.dummy.myerp.business.impl.manager;

import com.dummy.myerp.business.contrat.BusinessProxy;
import com.dummy.myerp.business.contrat.manager.ComptabiliteManager;
import com.dummy.myerp.business.impl.BusinessProxyImpl;
import com.dummy.myerp.business.impl.TransactionManager;
import com.dummy.myerp.consumer.dao.contrat.ComptabiliteDao;
import com.dummy.myerp.consumer.dao.impl.DaoProxyImpl;
import com.dummy.myerp.model.bean.comptabilite.*;
import com.dummy.myerp.technical.exception.FunctionalException;
import com.dummy.myerp.technical.exception.NotFoundException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.transaction.IllegalTransactionStateException;
import org.springframework.transaction.TransactionStatus;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ComptabiliteManagerImplTest {


    @InjectMocks
    BusinessProxyImpl businessProxy;

    @InjectMocks
    DaoProxyImpl daoProxy;
    @Mock
    ComptabiliteDao comptabiliteDao;
    @Mock
    TransactionManager transactionManager;

    private ComptabiliteManager manager;

    @BeforeEach
    void initEach() {
        businessProxy.getInstance(daoProxy, transactionManager);
        manager = businessProxy.getComptabiliteManager();
    }

    @Nested
    @DisplayName("addReference tests")
    class addReferenceTests {

        @Test
        @DisplayName("addReference with existing sequence")
        void addReferenceWithSequence() throws Exception {
            EcritureComptable ecritureComptable =
                    new EcritureComptable(
                            2,
                            new JournalComptable("AC",	"Achat"),
                            "VE-2016/00002",
                            new SimpleDateFormat("dd/MM/yyyy").parse("30/12/2016"),
                            "TMA Appli Xxx"
                    );
            List<SequenceEcritureComptable> sequenceEcritureComptableList = new ArrayList<>();
            sequenceEcritureComptableList.add(new SequenceEcritureComptable("AC", 2016, 2));

            when(daoProxy.getComptabiliteDao().getSequenceEcritureComptable("AC", 2016)).thenReturn(sequenceEcritureComptableList);

            manager.addReference(ecritureComptable);

            verify(daoProxy.getComptabiliteDao()).getSequenceEcritureComptable("AC", 2016);
        }

        @Test
        @DisplayName("addReference with new sequence")
        void addReferenceWithNewSequence() throws Exception {
            EcritureComptable ecritureComptable =
                    new EcritureComptable(
                            2,
                            new JournalComptable("AC",	"Achat"),
                            "VE-2016/00002",
                            new SimpleDateFormat("dd/MM/yyyy").parse("30/12/2016"),
                            "TMA Appli Xxx"
                    );
            List<SequenceEcritureComptable> sequenceEcritureComptableList = new ArrayList<>();

            when(comptabiliteDao.getSequenceEcritureComptable("AC", 2016)).thenReturn(sequenceEcritureComptableList);

            manager.addReference(ecritureComptable);

            verify(comptabiliteDao).getSequenceEcritureComptable("AC", 2016);
        }
    }

    @Test
    @DisplayName("isEquilibree returns true")
    void checkEcritureComptableUnit() throws Exception {
        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        vEcritureComptable.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("03/02/2017"));
        vEcritureComptable.setLibelle("Libelle");
        vEcritureComptable.setReference("AC-2017/00003");
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                                                                                 null, new BigDecimal(123),
                                                                                 null));
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(2),
                                                                                 null, null,
                                                                                 new BigDecimal(123)));

        assertThatCode(() -> { manager.checkEcritureComptableUnit(vEcritureComptable);}).doesNotThrowAnyException();
    }

    @Test
    void checkEcritureComptableUnitViolation() throws Exception {
        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();
        Assertions.assertThrows(FunctionalException.class, () -> {
            manager.checkEcritureComptableUnit(vEcritureComptable);
        });
    }

    @Test
    void checkEcritureComptableUnitRG2() throws Exception {
        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        vEcritureComptable.setDate(new Date());
        vEcritureComptable.setLibelle("Libelle");
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                                                                                 null, new BigDecimal(123),
                                                                                 null));
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(2),
                                                                                 null, null,
                                                                                 new BigDecimal(1234)));

        assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
            manager.checkEcritureComptableUnit(vEcritureComptable);
        }).withMessage("L'écriture comptable n'est pas équilibrée.");
    }

    @Test
    @DisplayName("checkEcritureComptable throws exception if it does not conform with RG3")
    void checkEcritureComptableUnitRG3() throws Exception {
        EcritureComptable vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        vEcritureComptable.setDate(new Date());
        vEcritureComptable.setLibelle("Libelle");
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                                                                                 null, new BigDecimal(123),
                new BigDecimal(123)));
        assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
            manager.checkEcritureComptableUnit(vEcritureComptable);
        }).withMessage("L'écriture comptable ne respecte pas les règles de gestion.");
    }

    @DisplayName("checkEcritureComptable conforms with RG5")
    @Nested
    class checkEcritureComptableRG5 {
        @Test
        @DisplayName("Should pass with correct JournalComptable and year")
        void checkEcritureComptableUnitRG5() throws Exception {
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2018"));
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.setReference("AC-2018/00003");
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, new BigDecimal(123),
                    null));
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, null,
                    new BigDecimal(123)));
            assertThatCode(() -> {
                manager.checkEcritureComptableUnit(vEcritureComptable);
            }).doesNotThrowAnyException();
        }

        @Test
        @DisplayName("Should fail with incorrect JournalComptable")
        void checkEcritureComptableUnitRG5JournalComptableError() throws Exception {
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2018"));
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.setReference("BQ-2018/00003");
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, new BigDecimal(123),
                    null));
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, null,
                    new BigDecimal(123)));
            assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
                manager.checkEcritureComptableUnit(vEcritureComptable);
            }).withMessage("Les donnes de la reference ne correspond pas au Journal Comptable associé");
        }

        @Test
        @DisplayName("Should fail with incorrect Year")
        void checkEcritureComptableUnitRG5YearError() throws Exception {
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2018"));
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.setReference("AC-2019/00003");
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, new BigDecimal(123),
                    null));
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, null,
                    new BigDecimal(123)));
            assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
                manager.checkEcritureComptableUnit(vEcritureComptable);
            }).withMessage("Les donnes de la reference ne correspond pas à l'année associé");
        }
    }

    @DisplayName("checkEcritureComptable conforms with RG6")
    @Nested
    class checkEcritureComptableRG6 {
        @Test
        @DisplayName("Throws exception if reference is empty")
        void checkEcritureComptableContextReferenceIsEmpty() throws Exception {
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2018"));
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.setReference(null);
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, new BigDecimal(123),
                    null));
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, null,
                    new BigDecimal(123)));

            assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
                manager.checkEcritureComptableContext(vEcritureComptable);
            }).withMessage("La référence de l'écriture comptable n'est pas valide");
        }

        @Test
        @DisplayName("Does not throw exception if unique")
        void checkEcritureComptableContextOk() throws Exception {
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2018"));
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.setReference("AC-2018/00003");
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, new BigDecimal(123),
                    null));
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, null,
                    new BigDecimal(123)));

            when(comptabiliteDao.getEcritureComptableByRef("AC-2018/00003")).thenThrow(NotFoundException.class);

            assertThatCode(() -> {
                manager.checkEcritureComptableContext(vEcritureComptable);
            }).doesNotThrowAnyException();

            verify(comptabiliteDao).getEcritureComptableByRef("AC-2018/00003");
        }

        @Test
        @DisplayName("Throws exception if not unique without id")
        void checkEcritureComptableContextThrowsExceptionOnExistingReference() throws Exception {
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2018"));
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.setReference("AC-2018/00003");
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, new BigDecimal(123),
                    null));
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, null,
                    new BigDecimal(123)));

            when(comptabiliteDao.getEcritureComptableByRef("AC-2018/00003")).thenReturn(vEcritureComptable);

            assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
                manager.checkEcritureComptableContext(vEcritureComptable);
            }).withMessage("Une autre écriture comptable existe déjà avec la même référence.");

            verify(comptabiliteDao).getEcritureComptableByRef("AC-2018/00003");
        }

        @Test
        @DisplayName("Throws exception if not unique with id")
        void checkEcritureComptableContextThrowsExceptionOnExistingReferenceWithId() throws Exception {
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setId(5);
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2018"));
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.setReference("AC-2018/00003");
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, new BigDecimal(123),
                    null));
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, null,
                    new BigDecimal(123)));

            EcritureComptable returnEcritureComptable = new EcritureComptable();
            returnEcritureComptable.setId(1);
            returnEcritureComptable.setReference("AC-2018/00003");

            when(comptabiliteDao.getEcritureComptableByRef("AC-2018/00003")).thenReturn(returnEcritureComptable);

            assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
                manager.checkEcritureComptableContext(vEcritureComptable);
            }).withMessage("Une autre écriture comptable existe déjà avec la même référence.");

            verify(comptabiliteDao).getEcritureComptableByRef("AC-2018/00003");
        }
    }

    @DisplayName("checkEcritureComptable Tests")
    @Nested
    class checkEcritureComptable {

        @Test
        @DisplayName("No exception should be thrown if EcritureComptable OK")
        void checkEcritureComptableOk() throws Exception {
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2018"));
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.setReference("AC-2018/00003");
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, new BigDecimal(123),
                    null));
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, null,
                    new BigDecimal(123)));

            when(comptabiliteDao.getEcritureComptableByRef("AC-2018/00003")).thenThrow(NotFoundException.class);

            assertThatCode(() -> {
                manager.checkEcritureComptable(vEcritureComptable);
            }).doesNotThrowAnyException();

            verify(comptabiliteDao).getEcritureComptableByRef("AC-2018/00003");
        }

        @Test
        @DisplayName("Throws error if first method fails")
        void checkEcritureComptable1stMethodFails() throws Exception {
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2018"));
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.setReference("AC-2018/00003");
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, new BigDecimal(123),
                    null));

            assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
                manager.checkEcritureComptable(vEcritureComptable);
            });
        }

        @Test
        @DisplayName("Throws error if second method fails")
        void checkEcritureComptable2ndMethodFails() throws Exception {
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2018"));
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.setReference("AC-2018/00003");
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, new BigDecimal(123),
                    null));
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, null,
                    new BigDecimal(123)));

            when(comptabiliteDao.getEcritureComptableByRef("AC-2018/00003")).thenReturn(vEcritureComptable);

            assertThatExceptionOfType(FunctionalException.class).isThrownBy(() -> {
                manager.checkEcritureComptable(vEcritureComptable);
            });

            verify(comptabiliteDao).getEcritureComptableByRef("AC-2018/00003");
        }
    }

    @DisplayName("insertEcritureComptable Tests")
    @Nested
    class insertEcritureComptableTests {

        @Test
        @DisplayName("Nominal case")
        void insertEcritureComptableOk() throws Exception {
            TransactionStatus transactionStatus = mock(TransactionStatus.class);
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2018"));
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.setReference("AC-2018/00003");
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, new BigDecimal(123),
                    null));
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, null,
                    new BigDecimal(123)));

            when(transactionManager.beginTransactionMyERP()).thenReturn(transactionStatus);
            when(comptabiliteDao.getEcritureComptableByRef("AC-2018/00003")).thenThrow(NotFoundException.class);
            doNothing().when(comptabiliteDao).insertEcritureComptable(vEcritureComptable);

            manager.insertEcritureComptable(vEcritureComptable);

            verify(comptabiliteDao).getEcritureComptableByRef("AC-2018/00003");
            verify(comptabiliteDao).insertEcritureComptable(vEcritureComptable);
            verify(transactionManager).commitMyERP(transactionStatus);
            verify(transactionManager).rollbackMyERP(null);
        }

        @Test
        @DisplayName("Rollback called when insert fails")
        void insertEcritureComptableKo() throws Exception {
            TransactionStatus transactionStatus = mock(TransactionStatus.class);
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2018"));
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.setReference("AC-2018/00003");
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, new BigDecimal(123),
                    null));
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, null,
                    new BigDecimal(123)));

            when(comptabiliteDao.getEcritureComptableByRef("AC-2018/00003")).thenThrow(NotFoundException.class);
            when(transactionManager.beginTransactionMyERP()).thenReturn(transactionStatus);
            doNothing().when(comptabiliteDao).insertEcritureComptable(vEcritureComptable);
            doThrow(IllegalTransactionStateException.class).when(transactionManager).commitMyERP(transactionStatus);

            assertThatExceptionOfType(IllegalTransactionStateException.class).isThrownBy(() -> {
                manager.insertEcritureComptable(vEcritureComptable);
            });

            verify(comptabiliteDao).getEcritureComptableByRef("AC-2018/00003");
            verify(comptabiliteDao).insertEcritureComptable(vEcritureComptable);
            verify(transactionManager).commitMyERP(transactionStatus);
            verify(transactionManager).rollbackMyERP(transactionStatus);
        }
    }

    @DisplayName("updateEcritureComptable Tests")
    @Nested
    class updateEcritureComptableTests {

        @Test
        @DisplayName("Nominal case")
        void updateEcritureComptableOk() throws Exception {
            TransactionStatus transactionStatus = mock(TransactionStatus.class);
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setId(999);
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2018"));
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.setReference("AC-2018/00003");
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, new BigDecimal(123),
                    null));
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, null,
                    new BigDecimal(123)));

            when(transactionManager.beginTransactionMyERP()).thenReturn(transactionStatus);
            when(comptabiliteDao.getEcritureComptableByRef("AC-2018/00003")).thenThrow(NotFoundException.class);
            doNothing().when(comptabiliteDao).updateEcritureComptable(vEcritureComptable);

            assertThatCode(() -> {
                manager.updateEcritureComptable(vEcritureComptable);
            }).doesNotThrowAnyException();

            verify(comptabiliteDao).updateEcritureComptable(vEcritureComptable);
            verify(transactionManager).commitMyERP(transactionStatus);
            verify(transactionManager).rollbackMyERP(null);
        }

        @Test
        @DisplayName("Rollback called when update fails")
        void updateEcritureComptableKo() throws Exception {
            TransactionStatus transactionStatus = mock(TransactionStatus.class);
            EcritureComptable vEcritureComptable = new EcritureComptable();
            vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
            vEcritureComptable.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("02/03/2018"));
            vEcritureComptable.setLibelle("Libelle");
            vEcritureComptable.setReference("AC-2018/00003");
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, new BigDecimal(123),
                    null));
            vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(1),
                    null, null,
                    new BigDecimal(123)));

            when(comptabiliteDao.getEcritureComptableByRef("AC-2018/00003")).thenThrow(NotFoundException.class);
            when(transactionManager.beginTransactionMyERP()).thenReturn(transactionStatus);
            doNothing().when(comptabiliteDao).updateEcritureComptable(vEcritureComptable);
            doThrow(IllegalTransactionStateException.class).when(transactionManager).commitMyERP(transactionStatus);

            assertThatExceptionOfType(IllegalTransactionStateException.class).isThrownBy(() -> {
                manager.updateEcritureComptable(vEcritureComptable);
            });

            verify(comptabiliteDao).updateEcritureComptable(vEcritureComptable);
            verify(transactionManager).commitMyERP(transactionStatus);
            verify(transactionManager).rollbackMyERP(transactionStatus);
        }
    }

    @DisplayName("deleteEcritureComptable Tests")
    @Nested
    class deleteEcritureComptableTests {

        @Test
        @DisplayName("Nominal case")
        void deleteEcritureComptableOk() throws Exception {
            TransactionStatus transactionStatus = mock(TransactionStatus.class);

            when(transactionManager.beginTransactionMyERP()).thenReturn(transactionStatus);
            doNothing().when(comptabiliteDao).deleteEcritureComptable(1);

            assertThatCode(() -> {
                manager.deleteEcritureComptable(1);
            }).doesNotThrowAnyException();

            verify(comptabiliteDao).deleteEcritureComptable(1);
            verify(transactionManager).commitMyERP(transactionStatus);
            verify(transactionManager).rollbackMyERP(null);
        }

        @Test
        @DisplayName("Rollback called when insert fails")
        void updateEcritureComptableKo() throws Exception {
            TransactionStatus transactionStatus = mock(TransactionStatus.class);

            when(transactionManager.beginTransactionMyERP()).thenReturn(transactionStatus);
            doNothing().when(comptabiliteDao).deleteEcritureComptable(1);
            doThrow(IllegalTransactionStateException.class).when(transactionManager).commitMyERP(transactionStatus);

            assertThatExceptionOfType(IllegalTransactionStateException.class).isThrownBy(() -> {
                manager.deleteEcritureComptable(1);
            });

            verify(comptabiliteDao).deleteEcritureComptable(1);
            verify(transactionManager).commitMyERP(transactionStatus);
            verify(transactionManager).rollbackMyERP(transactionStatus);
        }
    }

}

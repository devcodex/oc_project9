package com.dummy.myerp.business.impl;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.SimpleTransactionStatus;

import javax.swing.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TransactionManagerTest {

    @Mock
    PlatformTransactionManager platformTransactionManager;

    @Test
    @DisplayName("Checks if TransactionManager has single instance")
    void getInstanceSingleton() {
        TransactionManager instance1 = TransactionManager.getInstance(platformTransactionManager);
        TransactionManager instance2 = TransactionManager.getInstance();
        assertThat(instance1).isEqualTo(instance2);
    }

    @Test
    @DisplayName("beginTransaction Test")
    void beginTransactionMyERP() {

        ArgumentCaptor<DefaultTransactionDefinition> argumentCaptor = ArgumentCaptor.forClass(DefaultTransactionDefinition.class);

        when(platformTransactionManager.getTransaction(argumentCaptor.capture())).thenReturn(new SimpleTransactionStatus());

        TransactionManager transactionManager = TransactionManager.getInstance(platformTransactionManager);

        assertThat(transactionManager.beginTransactionMyERP()).isInstanceOf(SimpleTransactionStatus.class);

        verify(platformTransactionManager).getTransaction(argumentCaptor.capture());

        assertThat(argumentCaptor.getValue().getName()).isEqualTo("Transaction_txManagerMyERP");
        assertThat(argumentCaptor.getValue().getPropagationBehavior()).isEqualTo(TransactionDefinition.PROPAGATION_REQUIRED);

    }

    @Nested
    @DisplayName("commitMyERP tests")
    class commitMyERPTests {

        @Test
        @DisplayName("commit is called if transaction status not null")
        void commitMyERP() {
            TransactionManager transactionManager = TransactionManager.getInstance(platformTransactionManager);
            TransactionStatus transactionStatus = mock(TransactionStatus.class);

            doNothing().when(platformTransactionManager).commit(transactionStatus);

            transactionManager.commitMyERP(transactionStatus);

            verify(platformTransactionManager).commit(transactionStatus);
        }

        @Test
        @DisplayName("commit is not called if transaction status is null")
        void commitMyERPDoesNotCallCommit() {
            TransactionManager transactionManager = TransactionManager.getInstance(platformTransactionManager);
            TransactionStatus transactionStatus = null;

            transactionManager.commitMyERP(transactionStatus);

            verify(platformTransactionManager, never()).commit(transactionStatus);
        }
    }

    @Nested
    @DisplayName("commitMyERP tests")
    class rollbackMyERPTests {

        @Test
        @DisplayName("commit is called if transaction status not null")
        void commitMyERP() {
            TransactionManager transactionManager = TransactionManager.getInstance(platformTransactionManager);
            TransactionStatus transactionStatus = mock(TransactionStatus.class);

            doNothing().when(platformTransactionManager).rollback(transactionStatus);

            transactionManager.rollbackMyERP(transactionStatus);

            verify(platformTransactionManager).rollback(transactionStatus);
        }

        @Test
        @DisplayName("commit is not called if transaction status is null")
        void commitMyERPDoesNotCallCommit() {
            TransactionManager transactionManager = TransactionManager.getInstance(platformTransactionManager);
            TransactionStatus transactionStatus = null;

            transactionManager.rollbackMyERP(transactionStatus);

            verify(platformTransactionManager, never()).rollback(transactionStatus);
        }
    }
}

package com.dummy.myerp.business.impl;

import com.dummy.myerp.consumer.dao.impl.DaoProxyImpl;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class BusinessProxyImplTest {

    @InjectMocks
    private DaoProxyImpl daoProxy;

    @Mock
    TransactionManager transactionManager;

    private BusinessProxyImpl businessProxy;

    @Test
    @DisplayName("Checks if BusinessProxyImpl is initialized")
    void getInstance() {
        businessProxy = BusinessProxyImpl.getInstance(daoProxy,transactionManager);
        assertThat(businessProxy).isNotNull();
    }

    @Test
    @DisplayName("Checks if BusinessProxyImpl has single instance")
    void getInstanceSingleton() {
        BusinessProxyImpl instance1 = BusinessProxyImpl.getInstance(daoProxy,transactionManager);
        BusinessProxyImpl instance2 = BusinessProxyImpl.getInstance();
        assertThat(instance1).isEqualTo(instance2);
    }
}
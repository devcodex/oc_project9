package com.dummy.myerp.testbusiness.business;

import com.dummy.myerp.business.impl.BusinessProxyImpl;
import com.dummy.myerp.consumer.dao.impl.DaoProxyImpl;
import com.dummy.myerp.model.bean.comptabilite.*;
import com.dummy.myerp.technical.exception.FunctionalException;
import com.dummy.myerp.technical.exception.NotFoundException;
import org.junit.jupiter.api.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

public class BusinessIT {

    ApplicationContext ctxApplication = new
            ClassPathXmlApplicationContext("com/dummy/myerp/business/applicationContext-business.xml");

    BusinessProxyImpl businessProxy = ctxApplication.getBean(BusinessProxyImpl.class);
    DaoProxyImpl daoProxy = ctxApplication.getBean(DaoProxyImpl.class);

    @Test
    @DisplayName("getListCompteComptable")
    void getListCompteComptable() {

        List<CompteComptable> res = businessProxy.getComptabiliteManager().getListCompteComptable();
        assertThat(res).hasSize(7);
        for (CompteComptable each : res) {
            assertThat(each).hasNoNullFieldsOrProperties();
        }
    }

    @Test
    @DisplayName("getListJournalComptable")
    void getListJournalComptable() {
        List<JournalComptable> res = businessProxy.getComptabiliteManager().getListJournalComptable();
        assertThat(res).hasSize(4);
        for (JournalComptable each : res) {
            assertThat(each).hasNoNullFieldsOrProperties();
        }

    }

    @Test
    @DisplayName("getListEcritureComptable")
    void getListEcritureComptable() {
        List<EcritureComptable> res = businessProxy.getComptabiliteManager().getListEcritureComptable();

        assertThat(res).hasSize(5);
        for (EcritureComptable each : res) {
            assertThat(each).hasNoNullFieldsOrProperties();
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    class addReferenceIT {

        @BeforeEach
        void setup() {
            SequenceEcritureComptable resetSequence = new SequenceEcritureComptable("BQ", 2016, 51);
            daoProxy.getComptabiliteDao().updateSequenceEcritureComptable(resetSequence);
            System.out.println("SequenceEcritureComptable initialised to 51");
            //51
        }

        @Test
        @DisplayName("addReference")
        void addReference() throws ParseException {
            JournalComptable journalComptable = new JournalComptable("BQ", "Banque");
            List<EcritureComptable> listEcritureComptable = Arrays.asList(
                    new EcritureComptable(0, journalComptable, null, new SimpleDateFormat("dd/MM/yyyy").parse("08/9/2016"), "EC0"),
                    new EcritureComptable(1, journalComptable, null, new SimpleDateFormat("dd/MM/yyyy").parse("20/10/2016"), "EC1"),
                    new EcritureComptable(2, journalComptable, null, new SimpleDateFormat("dd/MM/yyyy").parse("30/11/2016"), "EC2")
            );

            int counter = 52;
            for (EcritureComptable each : listEcritureComptable) {
                businessProxy.getComptabiliteManager().addReference(each);
                assertThat(each.getReference()).isEqualTo("BQ-2016/000" + counter);
                counter++;
            }
        }

        @AfterEach
        void tearDown() {
            SequenceEcritureComptable resetSequence = new SequenceEcritureComptable("BQ", 2016, 51);
            daoProxy.getComptabiliteDao().updateSequenceEcritureComptable(resetSequence);
            System.out.println("SequenceEcritureComptable last value reset to 51");
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    @TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    class crudEcritureComptableIT {

        String reference;
        @BeforeAll
        void setup() {
            //Empty for now
        }

        @Test
        @Order(1)
        @DisplayName("insertEcritureComptable Test")
        void insertEcritureComptable() throws ParseException, FunctionalException, NotFoundException {
            EcritureComptable ecritureComptable = new EcritureComptable();
            ecritureComptable.setJournal(new JournalComptable("BQ", "Banque"));
            ecritureComptable.setDate(new SimpleDateFormat("dd/MM/yyyy").parse("03/02/2017"));
            ecritureComptable.setLibelle("Libelle");
            ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                    null, new BigDecimal(123),
                    null));
            ecritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(new CompteComptable(401),
                    null, null,
                    new BigDecimal(123)));

            businessProxy.getComptabiliteManager().addReference(ecritureComptable);

            reference = ecritureComptable.getReference();
            EcritureComptable tempEcritureComptable;
            assertThatExceptionOfType(NotFoundException.class).isThrownBy(() -> {
                daoProxy.getComptabiliteDao().getEcritureComptableByRef(reference);
            });

            assertThatCode(() -> { businessProxy.getComptabiliteManager().insertEcritureComptable(ecritureComptable);}).doesNotThrowAnyException();

            tempEcritureComptable = daoProxy.getComptabiliteDao().getEcritureComptableByRef(reference);

            assertThat(tempEcritureComptable).hasNoNullFieldsOrProperties();
        }

        @Test
        @Order(2)
        @DisplayName("updateEcritureComptable Test")
        void updateEcritureComptable() throws ParseException, FunctionalException, NotFoundException {
            EcritureComptable originalEcritureComptable = daoProxy.getComptabiliteDao().getEcritureComptableByRef(reference);
            EcritureComptable modifiedEcritureComptable = originalEcritureComptable;

            modifiedEcritureComptable.setLibelle("Libelle Changed");

            businessProxy.getComptabiliteManager().updateEcritureComptable(modifiedEcritureComptable);

            modifiedEcritureComptable = daoProxy.getComptabiliteDao().getEcritureComptable(originalEcritureComptable.getId());

            assertThat(modifiedEcritureComptable).hasFieldOrPropertyWithValue("libelle", "Libelle Changed");
            assertThat(modifiedEcritureComptable).hasFieldOrPropertyWithValue("reference", originalEcritureComptable.getReference());
        }

        @Test
        @Order(3)
        @DisplayName("deleteEcritureComptable Test")
        void deleteEcritureComptable() throws ParseException, FunctionalException, NotFoundException {
            EcritureComptable ecritureComptable = daoProxy.getComptabiliteDao().getEcritureComptableByRef(reference);

            businessProxy.getComptabiliteManager().deleteEcritureComptable(ecritureComptable.getId());

            assertThatExceptionOfType(NotFoundException.class).isThrownBy(() -> {
                daoProxy.getComptabiliteDao().getEcritureComptable(ecritureComptable.getId());
            });
        }

        @AfterAll
        void tearDown() throws NotFoundException {
            SequenceEcritureComptable resetSequence = new SequenceEcritureComptable("BQ", 2016, 51);
            daoProxy.getComptabiliteDao().updateSequenceEcritureComptable(resetSequence);
        }
    }
}